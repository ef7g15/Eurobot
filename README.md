# Eurobot #

This project is built to allow a robot to traverse and score points at the Eurobot 2017 challenge.

We are using the Arduino Mega with its associated language in addition to the MD25 motor controller, a pair of EMG30 motors and a 12 Volt 2 Ah battery.

We gained sponsorship from RS Components and were provided with materials by Boeing via the University of Southampton.

Updates regarding the project management and design process can be found on the blackboard website.

We'd like to thank the Southampton faculty for assisting us, RS Components and Boeing for sponsoring us, in addition to all open source projects we've drawn from.

## Acknowledgements ##
 -	James Henderson (encoder excerpts)
 -	Giuseppe Masino (SharpIR library)
 -	Micheal Margolis (Servo library)
 -	Colton Schlosser (interrupt function generation)
 -	In addition to the Arduino and Adafruit teams

## How to get this thing running ##
 -	Installing notepad++ arduino highlighting:
	+	Download notepad++ from here: https://notepad-plus-plus.org/
	+	Download the arduino language pack from here: https://sourceforge.net/projects/narduinoplugin/ and follow the instructions
	+	Open your files in notepad++ and change the "Language" option on the top bar to "Arduino" - near the bottom
	+	Everything should look pretty and you can press tab to autocomplete and indent the program correctly

 -	Modified servo library: REQUIRED FOR THE TIMER TO WORK
	+	Go to C:\Program Files (x86)\Arduino\libraries\Servo\src\avr or <equivalent mac path>/Arduino/libraries/Servo/src/avr and
			open the file "ServoTimers.h" in notepad++ or something similar
	+	Comment out (put // in front of) #define \_useTimer4, and delete "\_timer4" from the inside of the brackets for the FIRST enum
	+	Save and exit
	+	Copy and paste your code from the N++ environment into a new arduino sketch and compile/upload

 -	Editing the pins:
	+	Scroll down to the list of "#define"s beginning with "// Pins"
	+	Change the number behind the name of the pin you'd like to edit to where you want the new connection to go
	+	Save and copy/paste into the arduino IDE, then compile/upload

 -	Editing the track routine:
	+	Scroll down to the "void bigTrack(mirror){" function
	+	The program will perform each of the commands you give it in order, so if you'd like to insert a movement, just look
				at the existing routine and follow the same syntax
	+	Don't forget the semicolons!

## Changelog ##

 -	2017-02-18	
	+	TODO: Clean up, tab->space, begin looking at travelling salesman, sensor background implementation, cyl/ball map
 -	2017-02-19
	+	DONE: tab->space
	+	NOT POSS: sensor background implementation, hoover
	+	UPDATES: 2 robots, no hoover, not able to test
 -	2017-02-20
	+	DONE: Edit preparing for test, made compileable, reimplemented turn algorithm
	+	TODO: Test robot, consider recursive route algorithm, cyl/ball map, avoidance procedures, sensor implementation
	+	UPDATES: Test tomorrow, current code as movement demo ONLY
 -	2017-02-21
	+	DONE: Interim design review successfully completed, robot avaliable to test, £1000 in sponsorship from RS components gained
	+	TODO: Figure out problems caused by high velocity on motors (current draw?), create XY coordinate system and populate with points, reconsider EPSILON value
	+	UPDATES: Fixed error in angle adjustment function, removed extern on global variable, updated WHEEL_DISTANCE
 -	2017-02-25
	+	DONE: Resolved mirroring, uploaded laptop changes, Implemented adjust arc, reduced VELOCITY for testing
	+	TODO: Clean up arc code, Test adjust arc, Test on track
	+	UPDATES: Laptop did not upload files onto onedrive, uploaded project to gitlab
 -	2017-02-26
	+	DONE: Tested robot on track
	+	TODO: Fix robot on track
	+	UPDATES: Caster sticks, robot is not equipped to handle slip induced errors
 -	2017-03-05
	+	DONE: Specified additional method details to be completed
	+	TODO: Work with Zbigniew on the control of the Braccio, test sensor implementation, test capabilities on the track
	+	UPDATES: Computer broken - requires repair, wrong ultrasonic sensors ordered, sharp IR to be tested
 -	2017-03-06
	+	NOT POSS: Library collisions when compiling, Servo relies on ISR, causing a collision with the definitions of __vector_1
 -	2017-03-07
	+	DONE: Commented servo, etc. out to prevent collisions with ISR ---FOR TESTING ONLY---
 -	2017-03-09
	+	DONE: Progress made on XY.ino, plans to implement into main code soon, created Debug.ino allowing for measurement of the track using the robot itself, Some minor cleanup efforts made to code
	+	TODO: Continue testing robot, implement switches for L/R and robot type the design of the robot
	+	UPDATES: Arduino mega arriving soon, reimplementation of clock based functions will have to be done
 -	2017-03-17
	+	DONE: Tested ultrasonic sensors, added implementation to be tested tomorrow on final robot, modified interrupt to use timer5, modified servo library to NOT use timer5, Split eurobot files into 3 - generic, cylinder and ball collectors
	+	NOT POSS: XY.ino abandoned due to inability to selfcorrect without more advanced sensors
 -	2017-03-19
	+	DONE: Update algorithms, fix logical error in adjust routine, add debug lines (will be removed at programming finish)
 -	2017-03-20
	+	DONE: Implemented theoretical avoidance system
	+	TODO: Check signs on avoidance to right, test avoidance in situ, implement IR scanner, check if Ultrasonics interfere - if so - add a pin to pulse power when a reading is required
	+	Possibly check on recursive avoidance (several triangles with capability to recover from error)
 -	2017-03-22
	+	DONE: Debug added for avoidance system to check for logical error, Awaiting tests
 -	2017-03-24
	+	DONE: Changed isObstructed to explicitly return true|false, added delay during servo initialisation, modified ultrasonic reading to pulse the power to the pin - prevents unwanted interferance
	+	TODO: Update ball track, check avoidance system calling, add pulsing to the ultrasonic sensors
 -	2017-03-27
	+	DONE: Updated sharp IR model - now able to sense objects in the way, adjusted obstruction LB for ultrasonic, rewrote avoidance calculations, analysed code
	+	TODO: Fix forward rolling on the large bot - discover bug/wiring problem
 -	2017-03-28
	+	DONE: Found error cause - servos drawing too much current - mitigated with capacitors awaiting test, added colour sensor code
 -	2017-03-29
	+	DONE: Added ball motor relay logic - motor should turn off with everything else at 90 seconds
	+	TODO: Test, continue wiring of robot, get batteries and regulators to power the Braccio, begin refinement and review of methods
 -	2017-03-30
	+	DONE: Modified code to use adafruit motor shield, final day to test before middlesex
	+	UPDATES: Blew MD25 - DO NOT CONNECT THE BATTERY AND THE Vin JUMPER ON THE MOTOR SHIELD AT THE SAME TIME!!!!!!!!
 -	2017-03-31
	+	DONE: Removed dead weight, group decision made to no longer focus on smaller robot - modifications made appropriately, project files cut back - back to only 1 file (Eurobot.ino)
	+	TODO: Find adafruit motor shield - servo and wire library attributions to add to the header
 -	2017-04-26
	+	DONE: Scrapped old avoidance system - not useful in the given circumstances, major overhauls with robot design (1 IR forward, 1 US backward) requiring corresponding changes, added attributions
	+	TODO: Make adjustments for updated designs - will require overhaul
	+	UPDATES: Wired up regulator for the 5 V arm supply, massive design changes made - 2 module collector robots now running requiring corresponding changes
 -	2017-04-29
	+	DONE: Changed the launcher servo connection to take advantage of the unused m4 port on the braccio shield, added several functions to control the Braccio
	+	TODO: Debug launchLander never being run, complete new track methods
	+	UPDATES: Buck regulator blown - short circuit was caused on the Vout side, investigation into creating a buck regulator begun
 -	2017-04-30
	+	DONE: Fixed adjustLine and adjustAngle programs adding TIME_UP checks - launcher should now work
	+	TODO: Test on the track
	+	UPDATES: Parts list complete for 12 V -> 5 V buck regulator - will wait until tomorrow before ordering (Bank holiday allows for time to discuss)
 -	2017-05-02
	+	DONE: Added servo code for smaller robot
	+	TODO: Continue encoding track routines, test track routine in situ
	+	UPDATES: Delay on RSComponents order - expecting regulator components tomorrow
 -	2017-05-03
	+	DONE: Adjusted servo limits
	+	TODO: Complete regulator assembly (workshops closed before finishing), get new servo
 -	2017-05-04
	+	DONE: Regulator assembled
	+	TODO: Fix arduino power issues by splitting battery cable
 -	2017-05-05
	+	DONE: Got robot moving, added WHEEL_RADIUS changes dependent on robot, added split on the 12 V line to power the arduino from the barrel connector
	+	TODO: Complete strategy and implement into program
 -	2017-05-06
	+	DONE: Scored 4 points with the small robot
	+	TODO: Fix the larger robot having communication problems with the MD25, implement avoidance system, complete revised track strategy
 -	2017-05-08
	+	DONE: Implemented avoidance system, added further track improvements for the larger robot
	+	UPDATES: Final competition tomorrow, robot arm seems to be working BUT requires batteries charged up
 -	2017-05-09
	+	DONE: Scored 37 points with the robots
 -	2017-05-16
	+	DONE: Formatted the program to make it neater and remove unused components, moved the changelog and acknowledgements into the README.md file
	+	This will most probably be the last commit, my sincerest apologies for not uploading each change as it was made and I'm sure it's made the history quite messy :s.
	+	So long and thanks for all the fish
 -	2017-05-19
	+	Slight formatting changes, tab = 8 spaces, minor cleanup of the code preparing it to be printed out