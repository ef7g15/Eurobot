/*
	File: Debug.ino
	Group: 10
	Author:			Ed
	Creation:		2017-03-09
	Last Modified:	2017-03-09
	
	This program will provide continuous feedback to the serial monitor about the robot motion.
	It can measure angle turned (deg) and distance travelled (mm). Its output depends on the position of the debug switches.
	NOTE: PIN LOW MUST BE CONNECTED TO GROUND, OTHERWISE THE PROGRAM DOES NOT ACT AS INTENDED.
	TODO: separate definitions etc. into a seperate file to prevent repetition
*/

#include <Wire.h>

//	Command Bytes
#define MD25ADDRESS		0x58
#define SPEED1			(byte)0x00	// Byte to send forward velocity to the motors
#define SPEED2			0x01		// Byte to send turn velocity to the motors
#define ENCODER1		0x02		// Byte to read motor encoder 1
#define ENCODER2		0x06		// Byte to read motor encoder 2
#define CMD				0x10		// Byte to send encoder reset command
#define RESET_ENCODERS	0x20		// Byte to reset motor encoders
#define MODE_SELECTOR	0x0F		// Byte to change between control modes
#define ACC_SELECTOR	0x14		// Byte to read/change acceleration mode
#define DISABLE_TIMEOUT	0x32		// Byte to disable motor timeout - saves on constantly transmitting velocity to the MD25

#define MOTOR_MODE		0x03		// Signed turn mode
#define MOTOR_ACC		0x0A		// Highest possible acceleration

#define WHEEL_RADIUS	50		//	Radius of the wheel attached to the motor
#define WHEEL_DISTANCE	255		//	Distance between wheel centres

#define DEBUG_SWITCH_ONE		10		//	Pin attached to a button
#define DEBUG_SWITCH_TWO		11		//	Pin attached to a button

void setup(){
	Wire.begin();
	Serial.begin(9600);
	setMode(MOTOR_MODE);
	setAcc(MOTOR_ACC);
	disableTimeout();
	clearEncoders();	// Prevents initial errors by uncleared encoders on previous run
	pinMode(DEBUG_SWITCH_ONE, INPUT);
	pinMode(DEBUG_SWITCH_TWO, INPUT);	
}

void loop(){
	if(digitalRead(DEBUG_SWITCH_ONE) == HIGH && digitalRead(DEBUG_SWITCH_TWO) == HIGH){
		Serial.println("Encoders Cleared");
		clearEncoders();
		delay(1000);
	}
	else if(digitalRead(DEBUG_SWITCH_ONE) == HIGH && digitalRead(DEBUG_SWITCH_TWO) == LOW){
		Serial.println("Reading Distance:");
		delay(500);
		while(digitalRead(DEBUG_SWITCH_TWO) != HIGH){
			Serial.print("Distance: ");
			Serial.println(encToDist());
		}
	}
	else if(digitalRead(DEBUG_SWITCH_TWO) == HIGH && digitalRead(DEBUG_SWITCH_ONE) == LOW){
		Serial.println("Reading Angle:");
		delay(500);
		while(digitalRead(DEBUG_SWITCH_ONE) != HIGH){
			Serial.print("Angle: ");
			Serial.println(encToAngle());
		}
	}
}
double encToDist(){
	int encoder1 = getEncoder(ENCODER1);
	int encoder2 = getEncoder(ENCODER2);
	int avgEnc = (encoder1+encoder2)/2;
	return 2*PI*WHEEL_RADIUS*avgEnc/360.0;
}

double encToAngle(){
	int encoder1 = getEncoder(ENCODER1);
	int encoder2 = getEncoder(ENCODER2);
	int avgEnc = (encoder1-encoder2)/2;
	return (2.0*WHEEL_RADIUS*avgEnc)/WHEEL_DISTANCE;
}
long getEncoder(byte encoderByte) {			// Gets the encoder value corresponding to the given encoderByte
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(encoderByte);
	Wire.endTransmission();

	Wire.requestFrom(MD25ADDRESS, 4);	// Request 4 bytes from MD25
	while(Wire.available() < 4);		// Wait for 4 bytes to arrive
	long poss1 = Wire.read();	// First byte from encoder, HH
	poss1 <<= 8;
	poss1 += Wire.read();		// Second byte from encoder,HL
	poss1 <<= 8;
	poss1 += Wire.read();		// Third byte from encoder, LH
	poss1 <<= 8;
	poss1 +=Wire.read();		// Fourth byte from encoder,LL

	return poss1;
}
void setMode(byte mode) {		// Sets the MD25 operation mode
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(MODE_SELECTOR);
	Wire.write(mode);
	Wire.endTransmission();
}
void setAcc(byte mode)  {		// Sets the MD25 acceleration characteristics
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(ACC_SELECTOR);
	Wire.write(mode);
	Wire.endTransmission();
}
void clearEncoders()  {			// Clears both encoder registers
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(CMD);
	Wire.write(RESET_ENCODERS);
	Wire.endTransmission();
}
void motorSpeed(int v, int t) {           // Sets the motor forward and turn speeds
	Wire.beginTransmission(MD25ADDRESS);  // Forward velocity of both motors - Mode 3
	Wire.write(SPEED1);
	Wire.write(v);
	Wire.endTransmission();

	Wire.beginTransmission(MD25ADDRESS);  // Sets the turn velocity: added to outside motor, subtracted from inside motor - Mode 3
	Wire.write(SPEED2);
	Wire.write(t);
	Wire.endTransmission();
	delay(5);
}
void motorStop(){
	motorSpeed(0, 0);
}
signed int sign(double x){
	return ((x < 0) ? -1 : 1);
}
void disableTimeout(){
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(CMD);
	Wire.write(DISABLE_TIMEOUT);
	Wire.endTransmission();
}
