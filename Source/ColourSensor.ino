#include <Wire.h>
#include "Adafruit_TCS34725.h"

// our RGB -> eye-recognized gamma color
byte gammatable[256];


Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);

void setup() {
	Serial.begin(9600);
	Serial.println("Color View Test!");

	if (tcs.begin()) {
		Serial.println("Found sensor");
	} else {
		Serial.println("No TCS34725 found ... check your connections");
		while (1); // halt!
	}

	// thanks PhilB for this gamma table!
	// it helps convert RGB colors to what humans see
	for (int i=0; i<256; i++) {
		float x = i;
		x /= 255;
		x = pow(x, 2.5);
		x *= 255;
		gammatable[i] = x;
	}
}


void loop() {
	uint16_t clear, red, green, blue;
	double redp, bluep;

	tcs.setInterrupt(false);		// turn on LED

	delay(60);  // takes 50ms to read 
  
	tcs.getRawData(&red, &green, &blue, &clear);
	redp = clear/red;
	bluep = clear/blue;
	if(bluep > 5.0){
		Serial.println("Yellow");
	} else if(redp > 3.5) {
		Serial.println("White");
	} else {
		Serial.println("Blue");
	}

	tcs.setInterrupt(true);  // turn off LED
  
	/*Serial.print("C:\t"); Serial.print(clear);
	Serial.print("\tR:\t"); Serial.print(red);
	Serial.print("\tG:\t"); Serial.print(green);
	Serial.print("\tB:\t"); Serial.print(blue);*/
	
	

	// Figure out some basic hex code for visualization
	/*uint32_t sum = clear;
	float r, g, b;
	r = red; r /= sum;
	g = green; g /= sum;
	b = blue; b /= sum;
	r *= 256; g *= 256; b *= 256;*/
}
