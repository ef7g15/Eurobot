#include <Servo.h>
#include <Braccio.h>
#include "Adafruit_TCS34725.h"

#define DEBUG	true
#define MAX_GRIPPER_ROTATION	190

Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);	// Adafruit colour sensor
enum Colour { YELLOW, BLUE, WHITE };	// Colour enumerations for colour sensor

Servo base, shoulder, elbow, wrist_ver, wrist_rot, gripper;	// Servos required for Braccio
int m1, m2, m3, m5, m6;					// Braccio motor last positions

void setup(){
	Wire.begin();
	Serial.begin(9600);
	if(DEBUG){Serial.println("Wire, Serial OK");};
	
	Braccio.begin();
	if(DEBUG){Serial.println("Braccio OK");};
	
	boolean tcs_success = tcs.begin();
	if(DEBUG){Serial.print("Colour sensor attached, status:\t"); Serial.println(tcs_success);};
}
void loop(){
	searchColour(BLUE);
}

Colour checkColour(){	// Checks if the colour is yellow, blue or white and returns the colour
	uint16_t clear, red, green, blue;
	double redp, bluep;	// Parametrised red and blue values
	Colour retVal;	// Temporary variable to be able to turn off light after reading the colour

	tcs.setInterrupt(false);		// turn on LED

	delay(60);  // takes 50ms to read 

	tcs.getRawData(&red, &green, &blue, &clear);	// Recieve raw RGB+C data
	redp = clear/red;	// Assign parametric values
	bluep = clear/blue;
	if(bluep > 5.0){
		retVal = YELLOW;
	} else if(redp > 3.5) {
		retVal = WHITE;
	} else {
		retVal = BLUE;
	}
	
	tcs.setInterrupt(true);  // turn off LED
	if(DEBUG){Serial.print("Colour check:\t"); Serial.println(retVal);};
	return retVal;
}
void searchColour(Colour search){	// Returns when the colour passed to the function is seen by the colour sensor
	int i = 0;
	while((int)search != (int)checkColour && i <= MAX_GRIPPER_ROTATION){	// (int) cast required to use boolean relations without recreating them for the colour enum
		armRecordedMove(m1, m2, m3, m5, i);
		i++;
	}
	while((int)search != (int)checkColour && i >= 0){	// Colour not found on the first pass, reverse
		armRecordedMove(m1, m2, m3, m5, i);
		i--;
	}
	if ((int)search != (int)checkColour){	// Colour not found on either pass, retry
		searchColour(search);
	}
}
void armRecordedMove(int m1p, int m2p, int m3p, int m5p, int m6p){	// Moves the braccio and records it to a set of global variables - required as the servo cannot return positions
	m1 = m1p;
	m2 = m2p;
	m3 = m3p;
	m5 = m5p;
	m6 = m6p;
	Braccio.ServoMovement(20, m1p, m2p, m3p, 0, m5p, m6p);	// 20: step speed, 0: no 4th servo attached
	if(DEBUG){Serial.print("Braccio moved:\t"); Serial.print(m1); Serial.print("\t"); Serial.print(m2); Serial.print("\t"); Serial.print(m3); Serial.print("\t"); Serial.print(m5); Serial.print("\t"); Serial.println(m6);};
}