
#define U_S_DATA_1 51
#define U_S_DATA_2 43

void setup() {
	Serial.begin(9600);
	pinMode(U_S_DATA_1, INPUT);
	pinMode(U_S_DATA_2, INPUT);
}

void loop() {
	long pulse, in, cm;

	pulse = pulseIn(U_S_DATA_1, HIGH);
	in = pulse/147;
	cm = in*2.54;
	Serial.print("US1: ");
	Serial.println(cm);
	delay(100);

	pulse = pulseIn(U_S_DATA_2, HIGH);
	in = pulse/147;
	cm = in*2.54;
	Serial.print("US2: ");
	Serial.println(cm);
	delay(100);
}