/*
		File:				Eurobot.ino
		Group:				10
		Authors:			Ed, Ali, Angelo, Greg and Zbigniew
		Creation:			2017-02-17
		Last Modified:			2017-05-19

	The program to run the robot for the Eurobot 2017 competition for Southampton University group 10.
	Tab indent = 8 spaces.
	See README.md for changelog - Avaliable at https://gitlab.com/ef7g15/Eurobot
*/

//	Inclusions
#include <Wire.h>	// I2C library - Required for the MD25
#include <Servo.h>	// Required for both robots' grippers
#include <SharpIR.h>	// Required for the IR sensor on both robots

//	Defined Variables (all distances in mm unless otherwise specified)
#define WHEEL_RADIUS_BIG	50	// Radius of the wheels of the large robot
#define WHEEL_RADIUS_SMALL	49	// Radius of the wheels of the small robot
#define WHEEL_DISTANCE_BIG	253	// Distance between wheel centres for the large robot
#define WHEEL_DISTANCE_SMALL	180
#define VELOCITY_BIG		15	// Signed velocity value for the large robot (-128, 127)
#define VELOCITY_SMALL		30
#define EPSILON			2	// Minimum variation in encoder value permitted
#define TIME_LIMIT		90	// Seconds until time up
#define GRIPPER_OPEN_POSITION	150	// The open position for the small robot's claw
#define GRIPPER_CLOSED_POSITION	110	// The closed position for the small robot's claw
#define COLLISION_LB_BIG	200	// The lower bound of the distance in mm at which the robot will take action to avoid collisions for the larger robot
#define COLLISION_LB_SMALL	60	// Smaller requires a different lower bound due to differences between the robots

//	Command Bytes
#define MD25ADDRESS	0x58
#define SPEED1		(byte)0x00	// Byte to send forward velocity to the motors
#define SPEED2		0x01		// Byte to send turn velocity to the motors
#define ENCODER1	0x02		// Byte to read motor encoder 1
#define ENCODER2	0x06		// Byte to read motor encoder 2
#define CMD		0x10		// Byte to send encoder reset command
#define RESET_ENCODERS	0x20		// Byte to reset motor encoders
#define MODE_SELECTOR	0x0F		// Byte to change between control modes
#define ACC_SELECTOR	0x14		// Byte to read/change acceleration mode
#define DISABLE_TIMEOUT	0x32		// Byte to disable motor timeout - saves on constantly transmitting velocity to the MD25

#define MOTOR_MODE	0x03		// Signed turn mode
#define MOTOR_ACC	0x0A		// Highest possible acceleration

//	Pins
#define LEFT_RIGHT_PIN		7	// HIGH if the robot is starting in the top left, LOW if top right
#define SMALL_BIG_PIN		2	// HIGH to run the small robot track, LOW to run the large robot track
#define IR_PIN			A15	// The data output from the Sharp IR sensor
#define ULTRASONIC_PIN		18	// Data output from the ultrasonic sensor on the left side of the robot 	(PW)
#define SMALL_GRIPPER_PIN	9	// Attached to the small robot's servo input line
#define BIG_GRIPPER_PIN		4
#define START_PIN		22	// Starts HIGH, and when the circuit is broken, the robot will start moving
#define ROCKET_PIN		2	// The pin controlling the servo holding the rocket back

//	Global Variables (bad practise)
//		Timer Variables:
volatile int COUNTER = 0;		// Interrupt counter
volatile boolean TIME_UP = false;	// Is the time up (>90s)

//		Geometry Variables:
short WHEEL_RADIUS 	= WHEEL_RADIUS_BIG;	// Assume the larger robot is starting
short WHEEL_DISTANCE	= WHEEL_DISTANCE_BIG;
signed char VELOCITY 	= VELOCITY_BIG;
short COLLISION_LB 	= COLLISION_LB_BIG;

//		Key parameters:
boolean isSmallRobot;			// True if the smaller robot's program is to be executed
boolean isBlue;				// True if the robot is starting on the blue side
SharpIR sensor(GP2YA41SK0F, IR_PIN);	// IR sensor object "sensor" - distance in cm given by sensor.getDistance();

Servo rocket;	// Servo keeping the rocket in place on the big robot
Servo gripper;	// Servo connected to the gripper mechanisms on both robots


//	Methods
void setup(){
	Wire.begin();		// Start I2C library
	delay(20);
	
	setMode(MOTOR_MODE);
	setAcc(MOTOR_ACC);
	disableTimeout();
	motorStop();		// Stops the motor if it is still moving after a soft reset
	clearEncoders();	// Prevents initial errors by uncleared encoders on previous run

	pinMode(LEFT_RIGHT_PIN,	INPUT);
	pinMode(SMALL_BIG_PIN,	INPUT);
	pinMode(START_PIN,	INPUT);
	
	isSmallRobot 	= (digitalRead(SMALL_BIG_PIN) == HIGH);	// This boolean will select which routine is run
	isBlue 		= (digitalRead(LEFT_RIGHT_PIN) == LOW);	// This boolean will mirror the track based on which side it is starting on

	if(isSmallRobot){	// If we're running the small robot we need to set the correct geometry and servo pins
		VELOCITY 	= VELOCITY_SMALL;
		WHEEL_RADIUS 	= WHEEL_RADIUS_SMALL;
		WHEEL_DISTANCE 	= WHEEL_DISTANCE_SMALL;
		COLLISION_LB 	= COLLISION_LB_SMALL;
		
		gripper.attach(SMALL_GRIPPER_PIN);
		delay(20);	// Wait for the gripper to attach
		gripper.write(GRIPPER_OPEN_POSITION);
		delay(20);
	} else {		// Otherwise continue as normal and attach the large gripper pins
		gripper.attach(BIG_GRIPPER_PIN);
		delay(20);
		gripper.write(GRIPPER_OPEN_POSITION);
		delay(20);
		rocket.attach(ROCKET_PIN);
		delay(20);
		rocket.write(0);
		delay(20);
	}
	
	// Interrupt code generated with https://gist.github.com/cltnschlosser/e747efae07a76e117e02#file-ctc_timer_interrupts-py-L85
	cli();

	TCCR4A 	= 0;	//set timer4 interrupt at 1Hz (1.00)
	TCCR4B 	= 0;
	TCNT4 	= 0;

	OCR4A 	= 15624;
	TCCR4B |= (1 << WGM42);
	TCCR4B |= (1 << CS42) | (1 << CS40);
	TIMSK4 |= (1 << OCIE4A);

	sei();
}
ISR(TIMER4_COMPA_vect){							// Interrupt (called once a second)
	COUNTER ++;
	if (COUNTER >= TIME_LIMIT) TIME_UP = true;	// Set TIME_UP flag to break out of all movements
}
void loop(){
	// For the mirror image, lengths are the same, angles are just reversed, so we multiply all angles by a "mirror" parameter.
	char mirror = (isBlue ? -1 : 1);	// In the case we start on the blue side rather than the y, we must make all angles negative (mirror the track).
	
	while(digitalRead(START_PIN) == HIGH);	// Waits until START_PIN is brought low (i.e. when the wire connecting the +5 V to START_PIN is pulled out)
	
	// We must reset the timer after the start pin is pulled (otherwise the robot will stop early)
	COUNTER = 0;
	TIME_UP = false;

	if(isSmallRobot){	// Run a different routine depending on which robot is running the program
		smallTrack(mirror);
	} else {
		bigTrack(mirror);
	}
	
	while(!TIME_UP){
		motorStop();	// Hang until interrupt
	}
	if(!isSmallRobot) launchLander();	// Launch the lander for the funny action with the large robot
	while(true){
		motorStop();	// Hang after finishing
	}
}

void bigTrack(char mirror){						// The route for the larger  robot to follow
	// Initial module from the home monochrome rocket
	line(800);
	turn(mirror*(-55));
	line(300);
	turn(mirror*148);
	delay(1000);

	// Go back and forth between the rocket and the starting area
	for(int i=0; i<4; i++){
		line(180, false);	// false is specified in order to ignore the rocket
		collectModule();
		line(-180, false);
		turn(mirror*45);
		line(60, false);
		dropModule();
		line(-60, false);
		turn(mirror*-45);
	}
	
	// Move out of the way of the small robot
	line(-300);
	turn(180);
}
void smallTrack(char mirror){						// The route for the smaller robot to follow
	// Module out of the gate
	turn(mirror*5);
	line(260);
	collectModule();
	line(180);
	delay(8000);  		// Wait for the larger robot to pass
	turn(180);
	line(420);
	dropModule();
	line(-100, false);	// false is specified in order to ignore the smaller robot
	
	// Module by the home facing centre module bay
	turn(mirror*-190);
	line(935);
	collectModule();
	line(-100, false);
	turn(-175);
	delay(20000);
	line(935);
	dropModule();
	line(-600);
  
	// Module close to the centre of the home side
	turn(mirror*120);
	line(450);
	collectModule();
	turn(180);
	line(450);
	turn(mirror*60);
	line(450);
	dropModule();
	line(-300, false);
	turn(180);
	delay(2000);
	
	// Go back and forth between the rocket and the home base
	for(int i=0; i<4; i++){
		turn(mirror*-40);
		line(900);
		turn(mirror*-45);
		line(150);
		collectModule();
		line(-150, false);
		turn(mirror*-135);
		line(900);
		turn(mirror*40);
		line(200);
		dropModule();
		line(-200, false);
		turn(180);
	}
}

void collectModule(){							// Collects a module on the playing field
	delay(20);	// Delays in order to allow the servos to react to the commands
	gripper.write(GRIPPER_CLOSED_POSITION);
	delay(20);
}
void dropModule(){							// Places a module down sideways in front of the robot
	delay(20);
	gripper.write(GRIPPER_OPEN_POSITION);
	delay(20);
}
void launchLander(){							// Stops everything and launches the lander after 90 seconds
	motorStop();
	delay(20);
	rocket.write(110);	// Release the rocket
	delay(2000);		// Wait to ensure everything cleared properly
}

boolean isObstructed(boolean isForward){				// Returns true if the robot is obstructed (checks either the forward or backward facing sensor)
	if(isForward){
		return sensor.getDistance()*10 < COLLISION_LB;  // SharpIR sensor returns the distance in cm
	} else {
		return readUltrasonic() < COLLISION_LB;
	}
}
long readUltrasonic(){							// Returns the distance the ultrasonic is measuring in mm
	long pulse, in, cm;
	pulse = pulseIn(ULTRASONIC_PIN, HIGH);
	in = pulse/147;
	cm = in*2.54;
	
	return cm*10;
}
void avoidanceRoutine(double lastEnc){					// Attempts to avoid collisions, needs to know the last encoder value the robot tried to move
	boolean isForward = (lastEnc > 0);	// Was the last movement going forwards? Needed to know which sensor to check
	while(isObstructed(isForward));		// Hang until obstruction disappears
	
	double remainingEncoder = lastEnc - averageEnc();	// The change in encoder value required to reach the robot's original goal
	double dist = remainingEncoder*(2*PI*WHEEL_RADIUS)/360;
	line(dist);
}
double averageEnc(){							// Returns the average of the 2 encoders
	return 0.5*(getEncoder(ENCODER1)+getEncoder(ENCODER2));
}

void line(double x){							// Moves the robot forwards in a line for a distance x mm
	line(x, true);	// Refer to overloaded function - saves having to duplicate code
}
void line(double x, boolean sense){					// Moves the robot forwards in a line for a distance x mm, if sense is false - don't use the sensors
	clearEncoders();	// Wipe the slate clean
	
	double rev = x/(2*PI*WHEEL_RADIUS);	// The number of revolutions for each wheel to turn in order to reach a total distance travelled x
	double enc = 360*rev;			// The encoder value when the robot reaches a distance x mm away
  
	motorSpeed(sign(x)*VELOCITY, 0);
	if(sense){
		while(abs(getEncoder(ENCODER1)) < abs(enc) && abs(getEncoder(ENCODER2)) < abs(enc) && !TIME_UP && (!isObstructed(x > 0) || !isObstructed(x > 0)));  // x > 0 tells the program which sensor to check, double check sensor
	} else {
		while(abs(getEncoder(ENCODER1)) < abs(enc) && abs(getEncoder(ENCODER2)) < abs(enc) && !TIME_UP);  // Not sensing, so don't consult the sensors
	}
	motorStop();
	if (isObstructed(x > 0) && sense){	// If we are obstucted in the direction of motion AND we are sensing, we must have broken out of the loop early, avoid the obstacle
		avoidanceRoutine(enc);
	} else {				// Otherwise begin correcting for errors
		adjustLine((int)enc, VELOCITY, sense);
	}
}
void adjustLine(int idealEncoder, int velocity, boolean sense){  	// Adjusts for momentum-introduced errors (assuming no slip, no encoder failure) will ignore the sensors if sense is set to false
	int delta = idealEncoder - averageEnc();	// The adjustment required under ideal conditions to reach the final ideal encoder
	if(delta <= EPSILON || TIME_UP) return;		// Breaks out of recursion
	if(velocity >= VELOCITY/8) velocity /= 2;	// NB the velocity will halve each time without reaching 0
	
	clearEncoders();	// Clear encoders later as we needed to use them beforehand to judge how much the robot has over or undershot
	motorSpeed(sign(delta)*velocity, 0);
	if(sense){
		while(abs(getEncoder(ENCODER1)) < abs(delta) && abs(getEncoder(ENCODER2)) < abs(delta) && !TIME_UP && (!isObstructed(delta > 0) || !isObstructed(delta > 0)));  // Needs TIME_UP comparison to ensure exit when time is up
	} else {
		while(abs(getEncoder(ENCODER1)) < abs(delta) && abs(getEncoder(ENCODER2)) < abs(delta) && !TIME_UP);
	}
	motorStop();
	
	if(isObstructed(delta > 0) && sense){		// Cut out to the avoidance system if attempting to correct into another robot
		avoidanceRoutine(delta);
	} else {
		adjustLine(delta, velocity, sense);
	}
}
void turn(double angle){						// Turns clockwise by a given angle (degrees) about the center of the robot
	clearEncoders();	// Wipe the slate clean

	double enc = (WHEEL_DISTANCE*angle)/(2*WHEEL_RADIUS);	// The theoretical encoder value for the motors at the end of the turn

	motorSpeed(0, sign(angle)*VELOCITY);	// Start motors turning in opposite directions
	while(abs(getEncoder(ENCODER1)) < abs(enc) && abs(getEncoder(ENCODER2)) < abs(enc) && !TIME_UP);  // Wait until encoder has reached or exceeded the value to turn the given angle
	motorStop();
	
	adjustAngle((int)enc, VELOCITY);	// Correct if possible for any errors accrued during the turn
}
void adjustAngle(int idealEncoder, int velocity){			// Adjusts for momentum-introduced errors (assuming no slip, no encoder failure)
	// The adjustment required under ideal conditions to reach the final ideal encoder, Accounting for negative angles (e.g. turn(-90))
	int delta = ((idealEncoder < 0) ? idealEncoder - (getEncoder(ENCODER1)-getEncoder(ENCODER2))/2 : idealEncoder + (-getEncoder(ENCODER1)+getEncoder(ENCODER2))/2);
	if(abs(delta) <= EPSILON || TIME_UP) return;	// Breaks out of recursion
	if(velocity >= VELOCITY/8) velocity /= 2;	// NB the velocity will halve each time without reaching zero
	
	clearEncoders();	// Clear encoders later as we needed to use them beforehand to judge how much the robot has over or undershot
	motorSpeed(0, sign(delta)*velocity);
	while(abs(getEncoder(ENCODER1)) < abs(delta) && abs(getEncoder(ENCODER2)) < abs(delta) && !TIME_UP);
	motorStop();
	
	adjustAngle(delta, velocity);
}

void disableTimeout(){							// Disables motor timeout after not recieving a signal
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(CMD);
	Wire.write(DISABLE_TIMEOUT);
	Wire.endTransmission();
}

//	Excerpts from Arduino Odometry Group I project
//	Contains excerpts of code from James Henderson's "Arduino example for MD25 and LCD03" - 2012
long getEncoder(byte encoderByte){					// Gets the encoder value corresponding to the given encoderByte
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(encoderByte);
	Wire.endTransmission();

	Wire.requestFrom(MD25ADDRESS, 4);	// Request 4 bytes from MD25
	while(Wire.available() < 4);		// Wait for 4 bytes to arrive
	long poss1 = Wire.read();	// First byte from encoder, HH
	poss1 <<= 8;
	poss1 += Wire.read();		// Second byte from encoder,HL
	poss1 <<= 8;
	poss1 += Wire.read();		// Third byte from encoder, LH
	poss1 <<= 8;
	poss1 +=Wire.read();		// Fourth byte from encoder,LL

	return poss1;
}
void setMode(byte mode){						// Sets the MD25 operation mode
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(MODE_SELECTOR);
	Wire.write(mode);
	Wire.endTransmission();
}
void setAcc(byte mode){							// Sets the MD25 acceleration characteristics
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(ACC_SELECTOR);
	Wire.write(mode);
	Wire.endTransmission();
}
void clearEncoders(){							// Clears both encoder registers
	Wire.beginTransmission(MD25ADDRESS);
	Wire.write(CMD);
	Wire.write(RESET_ENCODERS);
	Wire.endTransmission();
}
void motorSpeed(int v, int t){		      				// Sets the motor forward and turn speeds
	if(TIME_UP){
		v = 0; t = 0;
	}
	Wire.beginTransmission(MD25ADDRESS);  // Forward velocity of both motors - Mode 3
	Wire.write(SPEED1);
	Wire.write(v);
	Wire.endTransmission();

	Wire.beginTransmission(MD25ADDRESS);  // Sets the turn velocity: added to outside motor, subtracted from inside motor - Mode 3
	Wire.write(SPEED2);
	Wire.write(t);
	Wire.endTransmission();
	
	delay(20);	// Wait for the motors to react
}
void motorStop(){							// Stops the motors attached to the MD25
	motorSpeed(0, 0);
	if(TIME_UP) launchLander();	// Additional check just in case
}
signed char sign(double x){						// Returns the sign of the argument (positive or negative)
	return ((x < 0) ? -1 : 1);
}